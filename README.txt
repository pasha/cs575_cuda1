This directory has the follow layout
* report.pdf 		- A detailed analysis of CUDA-1 assignment. Please read the report first !!!
* report (folder) 	- Latex sources for the report.
* plots (folder) 	- Experiments log files, gnuplot data files, and plots.
			  All the experiment were ran on "bananas" machine.
* Makefile		- Primary make file
* vecaddKernel01.cu	- Optimized vector add kernel
* matmultKernel01.cu	- Optimized matrix multiply kernel
