///
/// vecAddKernel00.cu
/// For CSU CS575 Spring 2011
/// Instructor: Wim Bohm
/// Based on code from the CUDA Programming Guide
/// By David Newman
/// Created: 2011-02-16
/// Last Modified: 2011-02-16 DVN
///
/// This Kernel adds two Vectors A and B in C on GPU
/// without using coalesced memory access.
/// 

__global__ void AddVectors(const float* A, const float* B, float* C, int N)
{
    int blockStartIndex  = blockIdx.x * blockDim.x * N;
    // Pavel's note: Thread start index is and ID of the trad in the block
    int threadStartIndex = blockStartIndex + threadIdx.x ;
    //int threadEndIndex   = threadStartIndex + N;
    // Pavel's note: The last index is the same for all threads - number of threads by N
    int blockEndIndex = blockStartIndex + blockDim.x * N;
    int i;
    // Pavel's note : Vector-add Loop 
    // Each threads start from own index and goes over the array in steps of blockDim.x 
    // Using this pattern all thread access an contiguous section of the memory and
    // take an advantage of the memory coalescing in hardware.
    for( i=threadStartIndex; i<blockEndIndex; i+=blockDim.x ){
        C[i] = A[i] + B[i];
    }
}
