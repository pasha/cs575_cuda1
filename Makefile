###
### CS575 and CS575dl Spring 2011
### Wim Bohm, instructor
### Makefile for CUDA1 assignment
### By Wim Bohm and Mike Hamilton
### Created: 2011-01-27 DVN
### Last Modified: March 2013 Mike H
###

##
## Create variables for use below
##


SDK_INSTALL_PATH :=  /usr/local/cuda
NVCC=$(SDK_INSTALL_PATH)/bin/nvcc
LIB       :=  -L$(SDK_INSTALL_PATH)/lib64 -L$(SDK_INSTALL_PATH)/samples/common/lib/linux/x86_64
INCLUDES  :=  -I$(SDK_INSTALL_PATH)/include -I$(SDK_INSTALL_PATH)/samples/common/inc
OPTIONS   :=  -O3 --maxrregcount=100 --ptxas-options -v --compiler-bindir  /usr/local/gcc-4.6.3/bin/gcc

TAR_FILE_NAME  := YourNameCUDA1.tar
EXECS :=  vecadd00 matmult00 vecadd01 matmult01 
all:$(EXECS)

#######################################################################
clean:
	rm -f $(EXECS) *.o

#######################################################################
tar:
	tar -cvf $(TAR_FILE_NAME) Makefile *.h *.cu *.pdf

#######################################################################
vecaddKernel00.o : vecaddKernel00.cu
	${NVCC} $< -c -o $@ $(OPTIONS)

vecadd00 : vecadd.cu vecaddKernel.h vecaddKernel00.o
	${NVCC} $< vecaddKernel00.o -o $@ $(LIB) $(INCLUDES) $(OPTIONS)


#######################################################################
vecaddKernel01.o : vecaddKernel01.cu
	${NVCC} $< -c -o $@ $(OPTIONS)

vecadd01 : vecadd.cu vecaddKernel.h vecaddKernel01.o
	${NVCC} $< vecaddKernel01.o -o $@ $(LIB) $(INCLUDES) $(OPTIONS)


#######################################################################
## Provided Kernel
matmultKernel00.o : matmultKernel00.cu matmultKernel.h 
	${NVCC} $< -c -o $@ $(OPTIONS)

matmult00 : matmult.cu  matmultKernel.h matmultKernel00.o
	${NVCC} $< matmultKernel00.o -o $@ $(LIB) $(INCLUDES) $(OPTIONS)


#######################################################################
## Expanded Kernel
matmultKernel01.o : matmultKernel01.cu matmultKernel.h
	${NVCC} $< -c -o $@ $(OPTIONS) -DFOOTPRINT_SIZE=32

matmult01 : matmult.cu  matmultKernel.h matmultKernel01.o
	${NVCC} $< matmultKernel01.o -o $@ $(LIB) $(INCLUDES) $(OPTIONS) -DFOOTPRINT_SIZE=32




