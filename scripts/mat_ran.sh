#!/bin/bash

m0=./matmult00
m1=./matmult01

for size in `echo 8 16 32 64 128 256`
do
    echo Vector Size: $size
    size1=`echo $size/2 | bc`
    $m0 $size
    $m1 $size1
done
