#!/bin/bash

vec0=./vecadd00 
vec1=./vecadd01 

for size in `echo 500 1000 2000 4000 8000`
do
    echo Vector Size: $size
    $vec0 $size
    $vec1 $size
done
