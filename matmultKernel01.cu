///
/// matmultKernel00.cu
/// For CSU CS575 Spring 2011
/// Instructor: Wim Bohm
/// Based on code from the CUDA Programming Guide
/// Modified by Wim Bohm and David Newman
/// Created: 2011-01-27
/// Last Modified: 2011-02-23 DVN
///
/// Multiplies two matrices using CUDA: A x B = C
///
/// Copy this file and modify the MatMultKernel device function for
/// each of your experiments. 
///

#include "matmultKernel.h"

// Pavel's note: The FOOTPRINT_SIZE passed as a "-D" in the makefile
// The default size is 32

// Define a gpu kernel to perform matrix multiplication
// of A x B = C.
__global__ void MatMulKernel(Matrix A, Matrix B, Matrix C){

  // matrix blocks
  float *Asub, *Bsub, *Csub;
  // Putting these into registers speeds access.
  int thread_row = threadIdx.y;
  int thread_col = threadIdx.x;
  int block_row = blockIdx.y;
  int block_col = blockIdx.x;

  // Each THREAD BLOCK computes one sub matrix Csub of C
  // EACH THREAD creates its own matrix descriptor Csub
  // Pavel's note: We allocate 32x32 sub section of the C matrix 
  Csub = &C.elements[C.stride * FOOTPRINT_SIZE * block_row + FOOTPRINT_SIZE * block_col];

  // Each thread computes one element of Csub in its copy of CValue
  // Pavel's note: Each thread calculates 4 values
  float Cvalue[2][2] = {0,0,0,0};

  // Loop over all sub matrices in block_row of A and block_col of B
  // required to compute Csub. Block multiply each pair of sub matrices
  // and accumulate results
  // Pavel's note: we calculate in blocks of FOOTPRINT_SIZE
  for (int m = 0;  m < (A.width / FOOTPRINT_SIZE); ++m){
    // Get Asub and Bsub descriptors
    Asub = &A.elements[A.stride * FOOTPRINT_SIZE * block_row + FOOTPRINT_SIZE * m];
    Bsub = &B.elements[B.stride * FOOTPRINT_SIZE * m + FOOTPRINT_SIZE * block_col];

    // Pavel's note: Copy ELEMENTS OF  ASub and Bsub into shared memory
    // EACH THREAD loads eight ELEMENTS of ASub and eight of Bsub

    // Notice: every thread declares shared_A and shared_B in shared memory
    //         even though a thread block has only one shared_A and one shared_B
    __shared__ float shared_A[FOOTPRINT_SIZE][FOOTPRINT_SIZE];
    __shared__ float shared_B[FOOTPRINT_SIZE][FOOTPRINT_SIZE];

    // Pavel's note: Each thread copies eight elements of shared_A and one element of shared_B.
    // The order of reads was carefully crafted in order to enable memory access coalescing
	
    shared_A[thread_row][thread_col] = Asub[thread_row * A.stride + thread_col];
    shared_B[thread_row][thread_col] = Bsub[thread_row * B.stride + thread_col];
    shared_A[thread_row][thread_col + BLOCK_SIZE] = Asub[thread_row * A.stride + thread_col + BLOCK_SIZE];
    shared_B[thread_row][thread_col + BLOCK_SIZE] = Bsub[thread_row * B.stride + thread_col + BLOCK_SIZE];
    shared_A[thread_row + BLOCK_SIZE][thread_col] = Asub[(thread_row + BLOCK_SIZE)* A.stride + thread_col];
    shared_B[thread_row + BLOCK_SIZE][thread_col] = Bsub[(thread_row + BLOCK_SIZE)* B.stride + thread_col];
    shared_A[thread_row + BLOCK_SIZE][thread_col + BLOCK_SIZE] = Asub[(thread_row + BLOCK_SIZE)* A.stride + thread_col + BLOCK_SIZE];
    shared_B[thread_row + BLOCK_SIZE][thread_col + BLOCK_SIZE] = Bsub[(thread_row + BLOCK_SIZE)* B.stride + thread_col + BLOCK_SIZE];

    // Synchronize to ensure all elements are read
    __syncthreads();

    // Pavel's note: Do an inproduct of shared_A and shared_B
    // In totat 4 multiplications
#pragma unroll
    for(int e=0; e<FOOTPRINT_SIZE; ++e) {
       Cvalue[0][0] += shared_A[thread_row][e] * shared_B[e][thread_col];
       Cvalue[0][1] += shared_A[thread_row][e] * shared_B[e][thread_col + BLOCK_SIZE];
       Cvalue[1][0] += shared_A[thread_row + BLOCK_SIZE][e] * shared_B[e][thread_col];
       Cvalue[1][1] += shared_A[thread_row + BLOCK_SIZE][e] * shared_B[e][thread_col + BLOCK_SIZE];
    }

    // Synchronize to ensure all Cvalues have been incremented
    // before reading in the next shared_A AND shared_B BLOCKS
    __syncthreads();
  }

  // Write Csub to GLOBAL memory.
  // Each thread writes its own cell value.
  Csub[thread_row * C.stride + thread_col] = Cvalue[0][0];
  Csub[thread_row * C.stride + thread_col + BLOCK_SIZE] = Cvalue[0][1];
  Csub[(thread_row + BLOCK_SIZE) * C.stride + thread_col] = Cvalue[1][0];
  Csub[(thread_row + BLOCK_SIZE) * C.stride + thread_col + BLOCK_SIZE] = Cvalue[1][1];
}
